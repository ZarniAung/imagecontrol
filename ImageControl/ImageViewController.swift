//
//  ImageViewController.swift
//  ImageControl
//
//  Created by zarniaung on 2/28/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
        @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var drop: DropMenuButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        segControl(0)
        drop.initMenu(["Item 1", "Item 2", "Item 3", "Item 4"], actions: [({ () -> (Void) in
        }), ({ () -> (Void) in
        }), ({ () -> (Void) in
        }), ({ () -> (Void) in
        })])
        // Do any additional setup after loading the view.
    }


    @IBAction func segControl(_ sender: Any) {
        if segmentedControl.selectedSegmentIndex == 0 {
            slider.isHidden = false
            drop.isHidden = true
            drop.table.isHidden = true
        }else{
            slider.isHidden = true
            drop.isHidden = false
            drop.table.isHidden = false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
