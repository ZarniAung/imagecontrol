//
//  QrBarViewController.swift
//  ImageControl
//
//  Created by zarniaung on 2/28/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import UIKit

class QrBarViewController: UIViewController {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var dropBarCode: DropMenuButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        qrBarSegControl(0)
        dropBarCode.initMenu(["Item 1", "Item 2", "Item 3", "Item 4"], actions: [({ () -> (Void) in
        }), ({ () -> (Void) in
        }), ({ () -> (Void) in
        }), ({ () -> (Void) in
        })])

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func qrBarSegControl(_ sender: Any) {
        if(segmentControl.selectedSegmentIndex == 0 ){
            dropBarCode.isHidden = true
            dropBarCode.table.isHidden = true
        }else{
            dropBarCode.isHidden = false
            dropBarCode.table.isHidden = false
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
